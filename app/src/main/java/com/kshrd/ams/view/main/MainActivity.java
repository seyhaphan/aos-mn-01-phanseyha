package com.kshrd.ams.view.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import com.kshrd.ams.R;
import com.kshrd.ams.view.home.HomeFragment;

public class MainActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container,HomeFragment.newInstance())
                .commit();


    }
}
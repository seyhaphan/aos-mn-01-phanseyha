package com.kshrd.ams.view.home;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.lifecycle.Observer;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.kshrd.ams.R;
import com.kshrd.ams.data.models.response.Article;
import com.kshrd.ams.data.models.response.ArticleResponse;
import com.kshrd.ams.data.models.response.DeleteArticleResponse;
import com.kshrd.ams.utils.OnItemViewClickListener;
import com.kshrd.ams.utils.RecyclerViewSwipeHelper;
import com.kshrd.ams.view.articledetail.ArticleDetailActivity;
import com.kshrd.ams.view.home.adapter.ArticleAdapter;
import com.kshrd.ams.view.home.homeviewmodel.HomeViewModel;
import com.kshrd.ams.view.forminput.FormInputFragment;
import com.paginate.Paginate;

import java.util.List;

public class HomeFragment extends Fragment{
    private static final String TAG = HomeFragment.class.getSimpleName() ;
    private RecyclerView recyclerViewArticles;
    private ArticleAdapter articleAdapter;
    private HomeViewModel viewModel;
    private SwipeRefreshLayout swipeRefresh;
    private FloatingActionButton btnPostArticle;
    private ArticleResponse articleResponse;
    private CoordinatorLayout coordinatorLayout;
    private boolean isLoading = false;
    private long currentPage = 1,limit = 15, totalPage;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        setupRecyclerView();
        setSwipeRefresh();

        viewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        viewModel.init();
        fetchArticle();

        setupPagination();

        btnPostArticle.setOnClickListener(v -> {
            FormInputFragment formInputFragment = new FormInputFragment("Save");
            Bundle bundle = new Bundle();

            bundle.putString("SAVE","Save");

            formInputFragment.setArguments(bundle);
            replaceFragment(formInputFragment);
        });



        RecyclerViewSwipeHelper recyclerViewSwipeHelper = new RecyclerViewSwipeHelper(getActivity(),recyclerViewArticles) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new RecyclerViewSwipeHelper.UnderlayButton(
                        "Delete",
                        R.drawable.ic_delete,
                        Color.parseColor("#FF3C30"),
                        new RecyclerViewSwipeHelper.UnderlayButtonClickListener() {
                            @Override
                            public void onClick(int pos) {
                                // TODO: onDelete

                                final Article deletedItem = articleAdapter.get(pos);
                                final int deletedIndex = pos;

                                // remove the item from recycler view
                                articleAdapter.removeItem(pos);

                                // showing snack bar with Undo option
                                Snackbar snackbar = Snackbar
                                        .make(coordinatorLayout, "Article removed", Snackbar.LENGTH_LONG);

                                snackbar.setAction("UNDO",v -> {
                                    Log.d(TAG, "onShown: ");
                                    // undo is selected, restore the deleted item
                                    articleAdapter.restoreItem(deletedItem, deletedIndex);
                                });

                                snackbar.addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                                      @Override
                                      public void onDismissed(Snackbar transientBottomBar, int event) {
                                          super.onDismissed(transientBottomBar, event);
                                          if(event == Snackbar.Callback.DISMISS_EVENT_TIMEOUT){
                                              viewModel.deleteArticleLiveData(deletedItem.getId())
                                                      .observe(getActivity(), new Observer<DeleteArticleResponse>() {
                                                  @Override
                                                  public void onChanged(DeleteArticleResponse deleteArticleResponse) {
                                                      Toast.makeText(getContext(),deleteArticleResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                                      Log.d(TAG, "onChanged: "+ deleteArticleResponse.getMessage());
                                                  }
                                              });
                                          }
                                      }
                                });

                                snackbar.setActionTextColor(Color.YELLOW);
                                snackbar.show();
                            }
                        }
                ));

                underlayButtons.add(new RecyclerViewSwipeHelper.UnderlayButton(
                        "Edit",
                        0,
                        Color.parseColor("#FF9502"),
                        new RecyclerViewSwipeHelper.UnderlayButtonClickListener() {
                            @Override
                            public void onClick(int pos) {
                                // TODO: OnTransfer
                                FormInputFragment formInputFragment = new FormInputFragment("Update");
                                Bundle bundle = new Bundle();

                                bundle.putString("UPDATE","Update");
                                Article article = articleAdapter.get(pos);
                                bundle.putSerializable("DATA_UPDATE",article);

                                formInputFragment.setArguments(bundle);
                                replaceFragment(formInputFragment);
                            }
                        }
                ));
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(recyclerViewSwipeHelper);
        itemTouchHelper.attachToRecyclerView(recyclerViewArticles);

    }

    private void replaceFragment(FormInputFragment formInputFragment) {
        getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.container, formInputFragment)
                .addToBackStack(null)
                .commit();
    }

    private void initView(@NonNull View view) {
        recyclerViewArticles = view.findViewById(R.id.recyclerViewArticles);
        recyclerViewArticles.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        swipeRefresh = view.findViewById(R.id.swipeRefresh);
        coordinatorLayout = view.findViewById(R.id.coordinator_layout);
        btnPostArticle = view.findViewById(R.id.btnPostArticle);
    }

    private void setupPagination() {
        isLoading = true;
        currentPage = 1;
        Paginate.with(recyclerViewArticles,callbacks)
                .addLoadingListItem(true)
                .setLoadingTriggerThreshold(2)
                .setLoadingListItemSpanSizeLookup(()-> 2)
                .build();
        callbacks.onLoadMore();
    }

    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            isLoading = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewModel.getArticleLiveData(currentPage,15).observe(getActivity(), response -> {
                        if(response != null){
                            totalPage = response.getPagination().getTotalPages();
                            articleResponse = response;
                            articleAdapter.setMoreDataSet(response);
                        }
                    });
                    currentPage++;
                    isLoading = false;
                }
            },1000);
            Log.d("TAG", "onLoadMore: "+currentPage);
        }

        @Override
        public boolean isLoading() {
            return isLoading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return currentPage == totalPage;
        }
    };

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.scroll_top_menu,menu);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.btnScrollTop ){
            recyclerViewArticles.smoothScrollToPosition(0);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setSwipeRefresh(){
        swipeRefresh.setOnRefreshListener(()->{
            totalPage = 0;
            currentPage = 1;
            articleAdapter.clear();
            fetchArticle();
            swipeRefresh.setRefreshing(false);
        });
    }

    private void fetchArticle(){
        viewModel.getArticleLiveData(currentPage,limit).observe(getActivity(), articleResponse -> {
            if(articleResponse != null){
                totalPage = articleResponse.getPagination().getTotalPages();
                this.articleResponse = articleResponse;
                articleAdapter.setDataSet(articleResponse);
            }
        });
    }

    private void setupRecyclerView() {
        articleAdapter = new ArticleAdapter(getContext(), new ArticleResponse(), new OnItemViewClickListener() {
            @Override
            public void onItemViewClick(View view, Article article) {
                Intent intent = new Intent(getContext(), ArticleDetailActivity.class);
                intent.putExtra("DATA",article);
                startActivity(intent);
            }
        });
        recyclerViewArticles.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewArticles.setAdapter(articleAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        currentPage = 1;
        totalPage = 0;
    }
}
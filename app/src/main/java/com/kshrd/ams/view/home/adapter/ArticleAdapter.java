package com.kshrd.ams.view.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kshrd.ams.R;
import com.kshrd.ams.data.models.response.Article;
import com.kshrd.ams.data.models.response.ArticleResponse;
import com.kshrd.ams.utils.OnItemViewClickListener;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder> {
    private OnItemViewClickListener onItemViewClickListener;
    private Context context;
    private ArticleResponse articleResponse;

    public ArticleAdapter(Context context, ArticleResponse articleResponse, OnItemViewClickListener onItemViewClickListener) {
        this.context = context;
        this.articleResponse = articleResponse;
        this.onItemViewClickListener = onItemViewClickListener;
    }

    public ArticleAdapter (Context context,ArticleResponse articleResponse){
        this.context = context;
        this.articleResponse = articleResponse;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_row, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Article article = articleResponse.getData().get(position);

        holder.articleTitle.setText(article.getTitle());
        Glide
            .with(context)
            .load(article.getImageUrl())
            .placeholder(R.drawable.placeholderimage)
            .centerCrop()
            .into(holder.articleImage);
    }

    @Override
    public int getItemCount() {
        return articleResponse.getData() == null ? 0 : articleResponse.getData().size();
    }

    public void setDataSet(ArticleResponse articleResponse){
        this.articleResponse = articleResponse;
        this.articleResponse.getData().clear();
        notifyDataSetChanged();
    }

    public void setMoreDataSet(ArticleResponse articleResponse){
        this.articleResponse.getData().addAll(articleResponse.getData());
        notifyDataSetChanged();
    }

    public void clear() {
        if(articleResponse.getData() != null){
            this.articleResponse.getData().clear();
            notifyDataSetChanged();
        }
    }

    public void removeItem(int position) {
        articleResponse.getData().remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Article item, int position) {
        articleResponse.getData().add(position, item);
        notifyItemInserted(position);
    }

    public Article get(int pos){
        Article  article = null;
        if(articleResponse.getData() != null)
             article = articleResponse.getData().get(pos);
        return article;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView articleTitle;
        ImageView articleImage;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            articleTitle = itemView.findViewById(R.id.text_title);
            articleImage = itemView.findViewById(R.id.image_view);

            itemView.setOnClickListener(v->{
                onItemViewClickListener.onItemViewClick(v,articleResponse.getData().get(getAdapterPosition()));
            });
        }
    }
}

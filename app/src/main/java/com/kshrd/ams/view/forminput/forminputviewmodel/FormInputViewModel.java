package com.kshrd.ams.view.forminput.forminputviewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kshrd.ams.data.models.request.ArticleRequest;
import com.kshrd.ams.data.models.response.PostArticleResponse;
import com.kshrd.ams.data.models.response.UpdateArticleResponse;
import com.kshrd.ams.data.models.response.UploadImageResponse;
import com.kshrd.ams.repositories.ArticleRepository;
import com.kshrd.ams.repositories.UploadImageRepository;

import java.io.File;

public class FormInputViewModel extends ViewModel {
    ArticleRepository articleRepository;
    UploadImageRepository uploadImageRepository;

    public void init(){
        articleRepository = new ArticleRepository();
        uploadImageRepository = new UploadImageRepository();
    }

    public MutableLiveData<PostArticleResponse> postArticleLiveData(ArticleRequest articleRequest){
       return articleRepository.postArticle(articleRequest);
    }

    public  MutableLiveData<UpdateArticleResponse> updateArticleLiveData(long id, ArticleRequest articleRequest){
        return articleRepository.updateArticle(id,articleRequest);
    }

    public MutableLiveData<UploadImageResponse> uploadImageLiveData(File file){
        return uploadImageRepository.uploadImage(file);
    }

}

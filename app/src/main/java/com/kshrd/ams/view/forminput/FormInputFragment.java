package com.kshrd.ams.view.forminput;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.kshrd.ams.R;
import com.kshrd.ams.data.models.request.ArticleRequest;
import com.kshrd.ams.data.models.response.Article;
import com.kshrd.ams.data.models.response.PostArticleResponse;
import com.kshrd.ams.data.models.response.UpdateArticleResponse;
import com.kshrd.ams.data.models.response.UploadImageResponse;
import com.kshrd.ams.view.forminput.forminputviewmodel.FormInputViewModel;

import java.io.File;

import static android.app.Activity.RESULT_OK;

public class FormInputFragment extends Fragment {
    private static final String TAG = FormInputFragment.class.getSimpleName();
    private static final int PICK_IMAGE_CODE = 101;
    private static final int PERMISSION_CODE = 1000;
    private Activity activity;
    private FormInputViewModel formInputViewModel;
    private EditText edTitle,edDescription;
    private ImageView imageView;
    private Button btnSave,btnCancel;
    private ProgressBar uploadProgressbar;
    private Uri imageUri;
    private String imageUrl;
    private String type;
    private long idUpdate;

    public FormInputFragment(String type){
        this.type = type;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edTitle = view.findViewById(R.id.edTitle);
        edDescription = view.findViewById(R.id.edDescription);
        imageView = view.findViewById(R.id.image_view_chooser);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        uploadProgressbar = view.findViewById(R.id.upload_progressbar);

        //request permission
        requestStoragePermission();

        if(type.equals("Save")){
            btnSave.setText("Save");
        }
        if(type.equals("Update")){
            btnSave.setText("Update");
            Bundle bundle = getArguments();
            Article article = (Article) bundle.getSerializable("DATA_UPDATE");
            idUpdate = article.getId();
            imageUrl = article.getImageUrl();
            edTitle.setText(article.getTitle());
            edDescription.setText(article.getDescription());
            Glide.with(activity)
                    .load(article.getImageUrl())
                    .centerCrop()
                    .placeholder(R.drawable.image01)
                    .into(imageView);

            idUpdate = article.getId();
        }

        formInputViewModel = ViewModelProviders.of(this).get(FormInputViewModel.class);
        formInputViewModel.init();

        btnCancel.setOnClickListener(v -> {
            activity.onBackPressed();
        });

        btnSave.setOnClickListener(v -> {
            if(btnSave.getText().equals("Save")){
                formInputViewModel.postArticleLiveData(getDataFromInput()).observe(getActivity(), new Observer<PostArticleResponse>() {
                    @Override
                    public void onChanged(PostArticleResponse postArticleResponse) {
                        Toast.makeText(activity,postArticleResponse.getMessage(),Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "onChanged: "+postArticleResponse.getMessage());
                    }
                });
            }else{
                formInputViewModel.updateArticleLiveData(idUpdate,getDataFromInput()).observe(getActivity(), new Observer<UpdateArticleResponse>() {
                    @Override
                    public void onChanged(UpdateArticleResponse updateArticleResponse) {
                        Toast.makeText(activity,updateArticleResponse.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
            }

        });

        imageView.setOnClickListener(v -> {
            showFileChooser();
        });

    }

    private ArticleRequest getDataFromInput() {
        String title = edTitle.getText().toString();
        String description = edDescription.getText().toString();

        ArticleRequest articleRequest = new ArticleRequest();
        articleRequest.setTitle(title);
        articleRequest.setDescription(description);
        articleRequest.setImageUrl(imageUrl);

        return articleRequest;
    }

    //method to show file chooser
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_CODE);
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = activity.getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

//    handling the image chooser activity result
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uploadProgressbar.setVisibility(View.VISIBLE);
            imageUri = data.getData();
//            imageView.setImageURI(imageUri);
            Glide.with(activity).load(imageUri).centerCrop().into(imageView);

            String path = getPath(imageUri);
            Uri uri = Uri.parse(path);
            File file = new File(uri.getPath());
            formInputViewModel.uploadImageLiveData(file).observe(this, new Observer<UploadImageResponse>() {
                @Override
                public void onChanged(UploadImageResponse uploadImageResponse) {
                    imageUrl = uploadImageResponse.getImageUrl();
                    uploadProgressbar.setVisibility(View.GONE);
                    Log.d(TAG, "onActivityResult: "+imageUrl);
                }
            });

        }
    }

    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {

        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_CODE);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(activity, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(activity, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }


}
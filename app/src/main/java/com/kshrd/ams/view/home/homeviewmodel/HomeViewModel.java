package com.kshrd.ams.view.home.homeviewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.kshrd.ams.data.models.response.ArticleResponse;
import com.kshrd.ams.data.models.response.DeleteArticleResponse;
import com.kshrd.ams.repositories.ArticleRepository;

public class HomeViewModel extends ViewModel {

    ArticleRepository articleRepository;

    public void init(){
        articleRepository = new ArticleRepository();
    }


    public MutableLiveData<ArticleResponse> getArticleLiveData(long page,long limit){
       return articleRepository.findAllArticle(page,limit);
    }


    public MutableLiveData<DeleteArticleResponse> deleteArticleLiveData(long id){
        return articleRepository.deleteArticleById(id);
    }
}
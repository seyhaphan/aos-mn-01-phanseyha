package com.kshrd.ams.view.articledetail;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kshrd.ams.R;
import com.kshrd.ams.data.models.response.Article;
import com.kshrd.ams.utils.OnItemViewClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ArticleDetailActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView edTitle,edDescription,edCreatedDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);

        imageView = findViewById(R.id.imageViewDetail);
        edTitle = findViewById(R.id.titleViewDetail);
        edDescription = findViewById(R.id.descriptionView);
        edCreatedDate = findViewById(R.id.createdDateView);

        Intent intent = getIntent();

        Article article = (Article) intent.getSerializableExtra("DATA");

        edTitle.setText(article.getTitle());
        edDescription.setText(article.getDescription());
        edCreatedDate.setText("Created Date: "+ getDateFormat(article.getDate()));
        Glide.with(this)
                .load(article.getImageUrl())
                .centerCrop()
                .placeholder(R.drawable.placeholderimage)
                .into(imageView);
    }

    private String getDateFormat(String date){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date dateFormat = null;
        try {
            dateFormat = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        formatter = new SimpleDateFormat("dd/MM/yyyy");
       return formatter.format(dateFormat);
    }

}
package com.kshrd.ams.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.kshrd.ams.data.models.request.ArticleRequest;
import com.kshrd.ams.data.models.response.ArticleResponse;
import com.kshrd.ams.data.models.response.DeleteArticleResponse;
import com.kshrd.ams.data.models.response.Pagination;
import com.kshrd.ams.data.models.response.PostArticleResponse;
import com.kshrd.ams.data.models.response.UpdateArticleResponse;
import com.kshrd.ams.data.remote.ArticleService;
import com.kshrd.ams.data.remote.RetrofitInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {
    private static final String TAG = ArticleRepository.class.getSimpleName();
    ArticleService articleService;

    public ArticleRepository(){
        articleService = RetrofitInstance.createService(ArticleService.class);
    }

    public MutableLiveData<ArticleResponse> findAllArticle(long page,long limit){
        MutableLiveData<ArticleResponse> postLiveData = new MutableLiveData<>();

        Call<ArticleResponse> listArticle = articleService.findAllArticle(page,limit);

        listArticle.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                if(response.isSuccessful() & response.body() != null){
                    postLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
        return postLiveData;
    }

    public MutableLiveData<PostArticleResponse> postArticle(ArticleRequest articleRequest){
        MutableLiveData<PostArticleResponse> postLiveData = new MutableLiveData<>();

        Call<PostArticleResponse> postArticle = articleService.postArticle(articleRequest);
        postArticle.enqueue(new Callback<PostArticleResponse>() {
            @Override
            public void onResponse(Call<PostArticleResponse> call, Response<PostArticleResponse> response) {
                if(response.isSuccessful() && response.body() != null){
                    postLiveData.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<PostArticleResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
        return postLiveData;
    }


    public MutableLiveData<UpdateArticleResponse> updateArticle(long id,ArticleRequest articleRequest){
        MutableLiveData<UpdateArticleResponse> postLiveData = new MutableLiveData<>();

        Call<UpdateArticleResponse> updateArticleResponseCall = articleService.updateArticle(id,articleRequest);

        updateArticleResponseCall.enqueue(new Callback<UpdateArticleResponse>() {
            @Override
            public void onResponse(Call<UpdateArticleResponse> call, Response<UpdateArticleResponse> response) {
                if(response.isSuccessful() && response.body() != null){
                    postLiveData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<UpdateArticleResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });

        return postLiveData;
    }


    public MutableLiveData<DeleteArticleResponse> deleteArticleById(long id){
        MutableLiveData<DeleteArticleResponse> postLiveData = new MutableLiveData<>();

        Call<DeleteArticleResponse> deleteArticleByIdCall = articleService.deleteArticleById(id);
        deleteArticleByIdCall.enqueue(new Callback<DeleteArticleResponse>() {
            @Override
            public void onResponse(Call<DeleteArticleResponse> call, Response<DeleteArticleResponse> response) {
                if(response.isSuccessful() & response.body() != null){
                    postLiveData.setValue(response.body());
                    Log.d(TAG, "onResponse: "+response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DeleteArticleResponse> call, Throwable t) {
                Log.d(TAG, "onResponse: "+t.getMessage());
            }
        });

        return postLiveData;
    }
}

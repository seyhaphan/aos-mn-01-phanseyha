package com.kshrd.ams.repositories;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.kshrd.ams.data.models.response.UploadImageResponse;
import com.kshrd.ams.data.remote.RetrofitInstance;
import com.kshrd.ams.data.remote.UploadImageService;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadImageRepository {
    private static final String TAG = UploadImageRepository.class.getSimpleName();
    UploadImageService uploadImageService;

    public UploadImageRepository(){
        uploadImageService = RetrofitInstance.createService(UploadImageService.class);
    }

    public MutableLiveData<UploadImageResponse> uploadImage(File file){
        MutableLiveData<UploadImageResponse> postLiveData = new MutableLiveData<>();

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part image =
                MultipartBody.Part.createFormData("file", file.getAbsolutePath(), requestFile);


        Call<UploadImageResponse> uploadImageCall = uploadImageService.uploadImage(image);

        uploadImageCall.enqueue(new Callback<UploadImageResponse>() {
            @Override
            public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                if(response.isSuccessful() && response.body() != null){
                    postLiveData.postValue(response.body());
                    Log.d(TAG, "uploadImage: "+response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });
        return postLiveData;
    }
}

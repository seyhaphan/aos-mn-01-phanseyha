package com.kshrd.ams.data.remote;

import com.kshrd.ams.data.models.request.ArticleRequest;
import com.kshrd.ams.data.models.response.ArticleResponse;
import com.kshrd.ams.data.models.response.DeleteArticleResponse;
import com.kshrd.ams.data.models.response.PostArticleResponse;
import com.kshrd.ams.data.models.response.UpdateArticleResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("v1/api/articles")
    Call<ArticleResponse> findAllArticle(@Query("page") long page,
                                         @Query("limit") long limit);

    @POST("v1/api/articles")
    Call<PostArticleResponse> postArticle(@Body ArticleRequest articleRequest);

    @PUT("v1/api/articles/{id}")
    Call<UpdateArticleResponse> updateArticle(@Path("id")long id ,@Body ArticleRequest articleRequest);

    @DELETE("v1/api/articles/{id}")
    Call<DeleteArticleResponse> deleteArticleById(@Path("id")long id);
}

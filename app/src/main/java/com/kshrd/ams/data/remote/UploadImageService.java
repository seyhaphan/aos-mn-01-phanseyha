package com.kshrd.ams.data.remote;

import com.kshrd.ams.data.models.response.UploadImageResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UploadImageService {

    @Multipart
    @POST("v1/api/uploadfile/single")
    Call<UploadImageResponse> uploadImage(@Part MultipartBody.Part image);
}

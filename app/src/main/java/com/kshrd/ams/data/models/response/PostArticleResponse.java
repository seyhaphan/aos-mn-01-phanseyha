package com.kshrd.ams.data.models.response;

import com.google.gson.annotations.SerializedName;

public class PostArticleResponse {
    private String code;
    private String message;
    @SerializedName("data")
    private Article article;

    public PostArticleResponse(){}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    @Override
    public String toString() {
        return "PostArticleResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", article=" + article +
                '}';
    }
}

package com.kshrd.ams.data.remote;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    private static final String BASE_URL = "http://110.74.194.124:15011/";
    private static final String API_KEY = "QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=";

    private static OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();

    private static Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass){
        okHttpClientBuilder.addInterceptor(new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder requestBuilder = request.newBuilder()
                        .addHeader("Authorization","Basic "+API_KEY)
                        .header("Accept","application/json")
                        .method(request.method(),request.body());
                return chain.proceed(requestBuilder.build());

            }
        });

        return retrofitBuilder
                .client(okHttpClientBuilder.build())
                .build()
                .create(serviceClass);
    }
}

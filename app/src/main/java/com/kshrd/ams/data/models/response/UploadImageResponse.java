package com.kshrd.ams.data.models.response;

import com.google.gson.annotations.SerializedName;

public class UploadImageResponse {
    private String code;
    private String message;
    @SerializedName("data")
    private String imageUrl;

    public UploadImageResponse(){}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "UploadImageResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}

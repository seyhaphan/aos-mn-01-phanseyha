package com.kshrd.ams.data.models.response;

import com.google.gson.annotations.SerializedName;

public class Pagination {
    private long page;
    private long limit;
    @SerializedName("total_count")
    private long totalCount;
    @SerializedName("total_pages")
    private long totalPages;

    public Pagination() {
        this.page = 1;
        this.limit = 15;
    }

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(long totalPages) {
        this.totalPages = totalPages;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "page=" + page +
                ", limit=" + limit +
                ", totalCount=" + totalCount +
                ", totalPages=" + totalPages +
                '}';
    }
}

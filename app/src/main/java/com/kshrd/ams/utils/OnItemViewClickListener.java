package com.kshrd.ams.utils;

import android.view.View;

import com.kshrd.ams.data.models.response.Article;

public interface OnItemViewClickListener {
    void onItemViewClick(View view, Article article);
}
